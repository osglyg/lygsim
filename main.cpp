#include <iostream>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <Windows.h>

#include "LYGFrameWork.h"
#include "LYGBasePlane.h"

using namespace std;

void InitData()
{
	LYGBasePlanePtr plane = boost::make_shared<LYGBasePlane>();
	plane->setPos(LYGPoint(12.5, 21.9));
	plane->setDestinationPos(LYGPoint(12000,14500));
	plane->setName("歼击机1");
	LYGBaseSensorPtr sensor = boost::make_shared<LYGBaseSensor>();
	sensor->setPlane(plane);
	sensor->setName("机载传感器1");
	plane->addSensor(sensor);

	LYGBaseSensorPtr sensor2 = boost::make_shared<LYGBaseSensor>();
	sensor2->setPlane(plane);
	sensor2->setName("机载传感器2");
	plane->addSensor(sensor2);

	LYGBasePlanePtr pl2 = boost::make_shared<LYGBasePlane>();

	pl2->setPos(LYGPoint(1000.0, 1000.0));
	pl2->setDestinationPos(LYGPoint(9000.0, 12000.0));
	pl2->setName("F35_2");
	LYGBaseSensorPtr s2 = boost::make_shared<LYGBaseSensor>();
	s2->setPlane(pl2);
	s2->setName("机载传感器3");
	pl2->addSensor(s2);

	LYGFrameWork::instance()->addPlane(plane); 
	LYGFrameWork::instance()->addPlane(pl2);
}
int main()
{
	InitData();
	LYGFrameWork::instance()->simStart();
	while (1)
	{ 
		LYGFrameWork::instance()->simRun(100);
		Sleep(100);
	}
	system("pause");
	return 0;
}