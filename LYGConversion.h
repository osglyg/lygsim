#pragma once
//-----------处理一些数据转换的公共函数
#include "LYGCommon.h"
#include "LYGPoint.h"

class LYGConversion
{
public:
	static LYGConversion * instance();

	// dir  方向 selfPos 基准点  head 以基准点为原点，正东为0，逆时针 ， pitch 以基准点为原点的俯仰
	void ComputeHeadPitch(LYGPoint dir, LYGPoint selfPos, double & head, double &pitch);
private:
	LYGConversion();
	~LYGConversion();
};