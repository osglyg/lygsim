#pragma once
#include <cmath>
#include "LYGCommon.h"

#define MINNUMBER 1.0e-6

class LYGPoint
{
public:
	LYGPoint();
	LYGPoint(const double x, const double y);
	LYGPoint(const LYGPoint & other);
	bool operator == (const LYGPoint & other);
	LYGPoint operator +(const LYGPoint & other);
	LYGPoint operator -(const LYGPoint & other);
	~LYGPoint();
public:
	//计算两点之间的距离
	double getDistance(const LYGPoint & other);
	//TODO 其它关于点的计算

	void SetX(const double x);
	double GetX() const;

	void SetY(const double y);
	double GetY() const;

private:
	double m_X;
	double m_Y;
};
