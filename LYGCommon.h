#pragma once
#include <iostream>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

#define CLASS_DECLARE_PTR(a)  typedef boost::shared_ptr<class a> a##Ptr;\
	typedef boost::weak_ptr<class a> a##WPtr


#define STRUCT_DECLARE_PTR(a)  typedef boost::shared_ptr<struct a> a##Ptr;\
	typedef boost::weak_ptr<struct a> a##WPtr

CLASS_DECLARE_PTR(LYGPoint);
CLASS_DECLARE_PTR(LYGBaseSensor);
CLASS_DECLARE_PTR(LYGBasePlane);


///雷达视场相关参数
struct SensorFovParam
{
	SensorFovParam()
	{
		maxRange = 20000;
		fovAzi = 360;
		fovAziPoint = 0;
		fovPitch = 60;
		fovPitchPoint = 0;
		antennaLength = 15;
	}
	//最大探测距离
	double maxRange;

	//探测视场方位范围
	double fovAzi;

	//方位指向
	double fovAziPoint;

	//天线长度
	double antennaLength;

	//探测视场俯仰范围
	double fovPitch;

	//俯仰指向
	double fovPitchPoint;
};

STRUCT_DECLARE_PTR(SensorFovParam);

struct SensorWorkParam
{
	SensorWorkParam()
	{
		sendPower = 1.0e6;
		antennaGain = 40.0;
		lamda = 0.056;
		rcs = 3.0;
		pulseN = 16.0;
		k = 1.0e23 / 1.38;
		t0 = 290.0;
		reciveFreqWidth = 1.6e6;
		reciveNoiseRadio = 10.0;
		reciveMinSignalRadio = 2.0;
	}
	//发射功率 w
	double sendPower; 

	//天线增益
	double antennaGain;

	//波长
	double lamda;

	//目标RCS
	double rcs;

	//脉冲累积数
	double pulseN;

	//波尔兹常数 取值为 1.38e-23J/K ，这里取倒数
	double k;

	//以绝对温度表示雷达接收机噪声温度
	double t0;

	//接收机通频带宽度
	double reciveFreqWidth;

	//接收机噪声系数
	double reciveNoiseRadio;

	//接收机最小信噪比
	double reciveMinSignalRadio;
};
STRUCT_DECLARE_PTR(SensorWorkParam);

//干扰设备参数
struct JammerParam
{
	JammerParam()
	{
		m_Power = 10.0;
		m_Gain = 10;
		m_BrandWidth = 2.0e6;
		m_PolaLoss = 0.5;
		m_Kj = 2.0;
		m_ThetaGain = 10.0;
		m_Range = 20000.0;
		m_HalfBrandWidth = 20.0;
	}

	double m_Power;       //发射功率
	double m_Gain;        //发射增益
	double m_ThetaGain;   //雷达波束扫描到theta接收增益
	double m_PolaLoss;    //极化损耗	
	double m_Range;       //与雷达的距离
	double m_BrandWidth;  //干扰的信号带宽
	double m_Kj;          //抗干扰压制系数
	double m_HalfBrandWidth;  //半功率波束宽度
};
STRUCT_DECLARE_PTR(JammerParam);

//机动参数
struct MotionParam
{
	MotionParam()
	{
		speed = 2;
		deltaTime = 0.1;
	}
	//速度
	double speed;
	
	//仿真步长,s
	double deltaTime;

	//TODO 后续补充其它参数
};
STRUCT_DECLARE_PTR(MotionParam);
