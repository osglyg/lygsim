#include "LYGBaseSensor.h"
#include "LYGFrameWork.h"

LYGBaseSensor::LYGBaseSensor()
	:m_BsePlane(nullptr)
{
	m_FovParam = boost::make_shared<SensorFovParam>();
	m_WorkParam = boost::make_shared<SensorWorkParam>();
}


LYGBaseSensor::~LYGBaseSensor()
{
	outFile.close();
}


void LYGBaseSensor::setPlane(LYGBasePlanePtr bsePlane)
{
	m_BsePlane = bsePlane;
}

LYGBasePlanePtr LYGBaseSensor::getPlane()
{
	return m_BsePlane;
}

void LYGBaseSensor::setName(std::string name)
{
	m_Name = name;
}

std::string LYGBaseSensor::getName()
{
	return m_Name;
}

void LYGBaseSensor::DetectTarget()
{
	//1.搜索场景中的所有目标 2.根据距离过滤目标 3.打印出目标的方位等信息
	auto planes = LYGFrameWork::instance()->getPlanes();
	for each (LYGBasePlanePtr var in planes)
	{
		//不是本机，则是其它目标，进行判断处理
		if (var->getName() != getPlane()->getName())
		{
			double tempDis = getPlane()->getPos().getDistance(var->getPos());
			//进入探测范围，需要将目标信息打印出来
			if (tempDis < m_FovParam->maxRange)
			{
				outFile << getName() << "探测目标：" << var->getName()
					<< "\t目标位置(X，Y)：(" << var->getPos().GetX() << "," 
					<< var->getPos().GetY() << ")\n"; 

			}
			std::cout << getName() << ",探测目标：" << var->getName() << "\t目标位置("
				<< var->getPos().GetX() << "," << var->getPos().GetY() << ")\n";
		}
	}
}

void LYGBaseSensor::start()
{
	std::string fileName = "../" + getName() + "探测结果.txt";
	outFile.open(fileName.c_str(), std::ios::out);
}
