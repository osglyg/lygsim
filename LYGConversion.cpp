#include "LYGConversion.h"

LYGConversion * LYGConversion::instance()
{
	static LYGConversion instance;
	return &instance;
}

void LYGConversion::ComputeHeadPitch(LYGPoint dir, LYGPoint selfPos, double & head, double & pitch)
{
	//TODO 如何计算拉，方法？
	pitch = 0.0;  //暂时不考虑三维 ，所以俯仰均为0

	double dis = dir.getDistance(LYGPoint(0.0, 0.0)); //相当于求出长度
	//通过余弦来计算角度
	double cosTheta = dir.GetX() / dis;
	head = acos(cosTheta);
}

LYGConversion::LYGConversion()
{

}

LYGConversion::~LYGConversion()
{
}
