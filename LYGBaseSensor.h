#pragma once

#include <fstream>
#include <iomanip>

#include "LYGCommon.h"
#include "LYGBasePlane.h"


class LYGBasePlane;    //相互包含

class LYGBaseSensor
{
public:
	LYGBaseSensor();
	~LYGBaseSensor();
public:
	//探测目标
	void DetectTarget();
	//仿真开始前的准备操作
	void start();
public:
	//设置挂载的飞机实体 ，后续换为基类
	void setPlane(LYGBasePlanePtr bsePlane);
	LYGBasePlanePtr getPlane();

	void setName(std::string name);
	std::string getName();
private:
	LYGBasePlanePtr m_BsePlane;
	//起始应该有个基类来管理类的名称、句柄等属性，后续可以考虑雷达、飞机都继承一个仿真对象基类
	std::string m_Name;
	//雷达视场参数
	SensorFovParamPtr m_FovParam;
	//雷达工作参数
	SensorWorkParamPtr m_WorkParam;

	//测试功能的输出
	std::ofstream outFile;
};
