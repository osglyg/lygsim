#pragma once

#include <vector>

#include "LYGPoint.h"
#include "LYGBaseSensor.h"

class LYGBaseSensor;

class LYGBasePlane
{
public:
	LYGBasePlane();
	~LYGBasePlane();

	bool operator == (const LYGBasePlane & other);
public:
	void addSensor(LYGBaseSensorPtr sensor);
	std::vector<LYGBaseSensorPtr> getSensors();
	void setPos(LYGPoint pos);
	LYGPoint getPos();
	void setName(std::string name);
	std::string getName();

	void setDestinationPos(LYGPoint destinationPos);
	LYGPoint getDestinationPos();

	//机动的接口
	void fly();

	void start();
private:
	//后续改成boost中智能指针
	std::vector<LYGBaseSensorPtr> m_SensorVec; 
	//当前位置
	LYGPoint m_Pos;
	//目的位置
	LYGPoint m_DestinationPos;
	std::string m_Name;
	MotionParamPtr m_MotionParam;
};
