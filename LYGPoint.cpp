#include "LYGPoint.h"



LYGPoint::LYGPoint()
	:m_X(0.0),m_Y(0.0)
{
}

LYGPoint::LYGPoint(const double x, const double y)
{
	m_X = x;
	m_Y = y;
}


LYGPoint::LYGPoint(const LYGPoint & other)
{
	m_X = other.m_X;
	m_Y = other.m_Y;
}

bool LYGPoint::operator == (const LYGPoint & other)
{
	if ((abs(m_X - other.m_X)< MINNUMBER) && (abs(m_Y - other.m_Y) < MINNUMBER))
	{
		return true;
	}
	return false;
}

LYGPoint LYGPoint::operator+(const LYGPoint & other)
{
	this->m_X += other.GetX();
	this->m_Y += other.GetY();
	return *this;
}

LYGPoint LYGPoint::operator-(const LYGPoint & other)
{
	this->m_X -= other.GetX();
	this->m_Y -= other.GetY();
	return *this;
}

LYGPoint::~LYGPoint()
{

}

double LYGPoint::getDistance(const LYGPoint & other)
{
	double dis = 0;
	dis = sqrt((m_X - other.m_X) * (m_X - other.m_X)
		+ (m_Y - other.m_Y) * (m_Y - other.m_Y));
	return dis;
}

void LYGPoint::SetX(const double x)
{
	m_X = x;
}

double LYGPoint::GetX() const
{
	return m_X;
}

void LYGPoint::SetY(const double y)
{
	m_Y = y;
}

double LYGPoint::GetY() const
{
	return m_Y;
}