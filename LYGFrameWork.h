#pragma once
#include "LYGCommon.h"
#include "LYGBasePlane.h"
#include "LYGBaseSensor.h"

class LYGFrameWork
{
public:
	static LYGFrameWork * instance();

	//添加数据
	void addPlane(LYGBasePlanePtr plane);
	std::vector<LYGBasePlanePtr> getPlanes();
	std::vector<LYGBaseSensorPtr> getSensors();
	//仿真控制
	void simStart();

	void simRun(long long time);
private:
	LYGFrameWork();
	~LYGFrameWork();

	std::vector<LYGBasePlanePtr> m_PlaneVec;
	std::vector<LYGBaseSensorPtr> m_SensorVec;
	long long  simTime;
};

