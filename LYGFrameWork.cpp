#include "LYGFrameWork.h"

LYGFrameWork * LYGFrameWork::instance()
{
	static LYGFrameWork instance;
	return &instance;
}

void LYGFrameWork::addPlane(LYGBasePlanePtr plane)
{
	m_PlaneVec.push_back(plane);
	auto sensors = plane->getSensors();
	m_SensorVec.insert(m_SensorVec.end(), sensors.begin(), sensors.end());
}

std::vector<LYGBasePlanePtr> LYGFrameWork::getPlanes()
{
	return m_PlaneVec;
}

std::vector<LYGBaseSensorPtr> LYGFrameWork::getSensors()
{
	return m_SensorVec;
}

void LYGFrameWork::simStart()
{
	//仿真时间为0	
	simTime = 0; 
	for each (LYGBasePlanePtr var in m_PlaneVec)
	{
		var->start();
	}
	for each (LYGBaseSensorPtr var in m_SensorVec)
	{
		var->start();
	}
}

void LYGFrameWork::simRun(long long time)
{
	simTime += time;
	for each (LYGBasePlanePtr var in m_PlaneVec)
	{
		var->fly();
	}
	for each (LYGBaseSensorPtr var in m_SensorVec)
	{
		var->DetectTarget();
	}
}

LYGFrameWork::LYGFrameWork()
{
	//TODO 后续看是否有预处理的操作
}


LYGFrameWork::~LYGFrameWork()
{
}
